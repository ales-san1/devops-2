FROM ubuntu as libbuild

ARG NGINX_VERSION=1.23.4

RUN apt-get update && \
	apt-get install -y --fix-missing		\
		build-essential		\
		libpcre3-dev		\
		wget			\
		zlib1g-dev		\
		pkg-config \
		libssl-dev \
		libssl1.0	\
		openssl
			
#RUN mkdir -p /tmp/openssllib 						&& \
#wget -q https://www.openssl.org/source/openssl-3.1.0.tar.gz	&& \
#	tar -xf openssl-3.1.0.tar.gz -C /tmp/openssllib		 				

		
RUN cd /tmp															&&	\
	wget -q http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && 	\
	tar -xf nginx-${NGINX_VERSION}.tar.gz
	
WORKDIR /tmp/nginx-${NGINX_VERSION}
# --without-http_gzip_module --with-zlib=/us --with-http_ssl_module --with-openssl=/tmp/openssllib/openssl-3.1.0
RUN   ./configure --with-ld-opt="-static -lpcre -lpthread" --with-http_ssl_module --with-http_v2_module	&&	\
	make install
	
FROM scratch

COPY --from=libbuild /etc/passwd /etc/group /etc/
COPY --from=libbuild /usr/local/nginx /usr/local/nginx

COPY nginx.conf /usr/local/nginx/conf

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/usr/local/nginx/sbin/nginx"]
CMD ["-g", "daemon off;"]