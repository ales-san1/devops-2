#!/bin/sh

#openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
#openssl rsa -passin pass:x -in server.pass.key -out server.key
#rm server.pass.key
#openssl req -new -key server.key -out server.csr \    -subj "/C=UK/ST=Warwickshire/L=Leamington/O=OrgName/OU=IT Department/CN=example.com"
#openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
mkdir -p /etc/nginx-certs/ssl/private 	&& \
mkdir -p /etc/nginx-certs/ssl/certs 	&& \
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx-certs/ssl/private/nginx-selfsigned.key -out /etc/nginx-certs/ssl/certs/nginx-selfsigned.crt -subj "/C=UK/ST=Warwickshire/L=Leamington/O=OrgName/OU=IT Department/CN=example.com"